<?php

namespace App\Form;

use App\Entity\Post;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo', TextType::class)
            ->add('imagen', FileType::class)
            ->add('texto', TextareaType::class)
            ->add('tipo', ChoiceType::class, [
                'choices' => [
                    'ELIGE UNA OPCION' => 'EVENTO_GENERICO',
                    'EVENTO GENERICO' => 'EVENTO_GENERICO',
                    'EVENTO COSPLAY' => 'EVENTO_COSPLAY',
                    'EVENTO PELÍCULA' => 'EVENTO_PELICULA',
                    'EVENTO REUNION' => 'EVENTO_REUNION',
                    'EVENTO COMIDA' => 'EVENTO_COMIDA'
                ]
        ])
            ->add('aforo', IntegerType::class)
            ->add('fecha', DateTimeType::class, [
                'format' => 'yyyy HH:mm:ss dd-MM',
                'input'  => 'datetime',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);
    }
}
