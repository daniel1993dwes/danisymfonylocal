<?php

namespace App\Controller;

use App\Entity\Inscripciones;
use App\Entity\Post;
use App\Form\InscripcionesType;
use App\Repository\InscripcionesRepository;
use App\Repository\PostRepository;
use App\Repository\UserRepository;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/inscripciones")
 */
class InscripcionesController extends AbstractController
{
    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/", name="inscripciones_index", methods={"GET"})
     */
    public function index(InscripcionesRepository $inscripcionesRepository): Response
    {
        return $this->render('inscripciones/index.html.twig', [
            'inscripciones' => $inscripcionesRepository->findAll(),
        ]);
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/new/{id}", name="inscripciones_new", methods={"GET","POST"})
     */
    public function new(Request $request, PostRepository $postRepository, UserRepository $userRepository, Post $post): Response
    {
        $inscripcione = new Inscripciones();
        $user = $userRepository->find($this->getUser());

        $fecha = new DateTime();
        $fecha->format('Y-m-d H:i:s');

        $inscripcione->setUser($user);
        $inscripcione->setPost($post);
        $inscripcione->setUsername($user->getUsername());
        $inscripcione->setFecha(new DateTime());

        $form = $this->createForm(InscripcionesType::class, $inscripcione);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($inscripcione);
            $entityManager->flush();

            return $this->redirectToRoute('inscripciones_index');
        }

        return $this->render('inscripciones/new.html.twig', [
            'inscripcione' => $inscripcione,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/{id}", name="inscripciones_show", methods={"GET"})
     */
    public function show(Inscripciones $inscripcione): Response
    {
        return $this->render('inscripciones/show.html.twig', [
            'inscripcione' => $inscripcione,
        ]);
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/{id}/edit", name="inscripciones_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Inscripciones $inscripcione): Response
    {
        $form = $this->createForm(InscripcionesType::class, $inscripcione);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('inscripciones_index', [
                'id' => $inscripcione->getId(),
            ]);
        }

        return $this->render('inscripciones/edit.html.twig', [
            'inscripcione' => $inscripcione,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/{id}", name="inscripciones_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Inscripciones $inscripcione): Response
    {
        if ($this->isCsrfTokenValid('delete'.$inscripcione->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($inscripcione);
            $entityManager->flush();
        }

        return $this->redirectToRoute('inscripciones_index');
    }
}
