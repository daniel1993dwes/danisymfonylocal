<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Helper\FileUploader;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="user_index", methods={"GET"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="user_new", methods={"GET","POST"})
     */
    public function new(Request $request, FileUploader $fileUploader): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $imagen */
            $imagen = $user->getAvatar();

            $fileName = $fileUploader->upload($imagen);
            $user->setAvatar($fileName);

            $user->setIsActive(true);
            $user->setRole('ROLE_USER');

            $user->setPassword(password_hash($user->getPassword(), PASSWORD_BCRYPT, ['cost' => 12]));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/{id}", name="user_show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        if ($this->getUser() !== $user)
        {
            throw new AccessDeniedException('Access Denied User!! Security vulnerability has been controlled.');
        }

        return $this->render('user/show.html.twig', [
            'user' => $user,
            'error' => null
        ]);
    }

    /**
 * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
 * @Security("has_role('ROLE_USER')")
 */
    public function edit(Request $request, User $user, FileUploader $fileUploader): Response
    {
        if ($this->getUser() !== $user)
        {
            throw new AccessDeniedException('Access Denied User!! Security vulnerability has been controlled.');
        }

        $user->setAvatar(new File(
            $this->getParameter('images_directory').'/'.$user->getAvatar()));

//        $image = $form->get('image')->getData();
//        $fileName = $fileUploader->upload($image);


        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $imagen */
            $imagen = $user->getAvatar();

            $fileName = $fileUploader->upload($imagen);
            $user->setAvatar($fileName);


            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index', [
                'id' => $user->getId(),
            ]);
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     * @Security("has_role('ROLE_USER')")
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->getUser() !== $user)
        {
            throw new AccessDeniedException('Access Denied User!! Security vulnerability has been controlled.');
        }

        if (count($user->getPosts()) > 0)
        {
            $this->addFlash('error', 'You have posts actives');
            return $this->render('user/show.html.twig', [
                'error' => 'You have posts actives',
                'user' => $user
            ]);
        }
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_index');
    }

    /**
     * @Route("/{id}/editRole", name="user_editRole", methods={"GET","POST"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editRole(Request $request, User $user)
    {
        $user->setIsActive($request->request->get('active'));
        $user->setRole($request->request->get('tipo'));

        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('user_index', [
            'id' => $user->getId(),
        ]);
    }
}
