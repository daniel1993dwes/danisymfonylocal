<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\User;
use App\Form\PostType;
use App\Helper\FileUploader;
use App\Repository\PostRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class PostController extends AbstractController
{
    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function indexAbsolute(PostRepository $postRepository): Response
    {
        $posts = $postRepository->findAll();

        return $this->render('index.html.twig', [
            'posts' => $posts,
            'error' => null
        ]);
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/post", name="post_index", methods={"GET"})
     */
    public function index(PostRepository $postRepository): Response
    {
        $posts = $postRepository->findAll();

        return $this->render('post/index.html.twig', [
            'posts' => $posts,
            'error' => null
        ]);
    }

    /**
     * @Route("/filter", name="filter_inicio", methods={"GET", "POST"})
     */
    public function filtrarInicio(PostRepository $postRepository, Request $request)
    {
        $fecha1 = $request->request->get('fecha1');
        $fecha2 = $request->request->get('fecha2');
        $tipo = $request->request->get('tipo');
        $texto = $request->request->get('texto');
        $posts = $postRepository->filtrosComplejosInicio($fecha1, $fecha2, $tipo, $texto);

        return $this->render('index.html.twig', [
            'posts' => $posts,
            'error' => 'Filtrando'
        ]);
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/post/filter", name="post_filter", methods={"GET", "POST"})
     */
    public function filtrar(PostRepository $postRepository, Request $request)
    {
        $fecha1 = $request->request->get('fecha1');
        $fecha2 = $request->request->get('fecha2');
        $tipo = $request->request->get('tipo');
        $texto = $request->request->get('texto');
        $usuario = $this->getUser();
        $posts = $postRepository->filtrarInicio($fecha1, $fecha2, $tipo, $texto, $usuario);


        return $this->render('post/index.html.twig', [
            'posts' => $posts,
            'error' => 'Filtrando'
        ]);
    }

    /**
     * @Route("/filter/{id}", name="filter_absolute", methods={"GET", "POST"})
     */
    public function filtrarAbsolute(PostRepository $postRepository, Request $request, User $user)
    {
        if ($user)
        {
            $filtroUser = $user;
        }
        else
        {
            $filtroUser = $this->getUser() ?? null;
        }
        $fecha1 = $request->request->get('fecha1');
        $fecha2 = $request->request->get('fecha2');
        $tipo = $request->request->get('tipo');
        $texto = $request->request->get('texto');
        $usuario = $filtroUser;
        $posts = $postRepository->filtrosComplejos($fecha1, $fecha2, $tipo, $texto, $usuario);

        return $this->render('index.html.twig', [
            'posts' => $posts,
            'error' => 'Filtrando'
        ]);
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/post/new", name="post_new", methods={"GET","POST"})
     */
    public function new(Request $request, FileUploader $fileUploader): Response
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $imagen */
            $imagen = $post->getImagen();

            $fileName = $fileUploader->upload($imagen);
            $post->setImagen($fileName);

            $post->setUser($this->getUser());
            $post->setCommentCant(0);
            $post->setVisible(true);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($post);
            $entityManager->flush();

            return $this->redirectToRoute('post_index');
        }

        return $this->render('post/new.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/post/{id}", name="post_show", methods={"GET"})
     */
    public function show(Post $post): Response
    {
        return $this->render('post/show.html.twig', [
            'post' => $post,
        ]);
    }

    /**
     * @Route("/post/{id}/edit", name="post_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Post $post, FileUploader $fileUploader): Response
    {
        $post->setImagen(new File(
            $this->getParameter('images_directory').'/'.$post->getImagen()));

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $imagen */
            $imagen = $post->getImagen();

            $fileName = $fileUploader->upload($imagen);
            $post->setImagen($fileName);

            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('post_index', [
                'id' => $post->getId(),
            ]);
        }

        return $this->render('post/edit.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/post/{id}", name="post_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Post $post): Response
    {
        if ($this->isCsrfTokenValid('delete'.$post->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($post);
            $entityManager->flush();
        }

        return $this->redirectToRoute('post_index');
    }

    public function montarQueryFiltrado()
    {

    }
}
