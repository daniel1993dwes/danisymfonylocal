<?php

namespace App\Repository;

use App\Entity\Inscripciones;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Inscripciones|null find($id, $lockMode = null, $lockVersion = null)
 * @method Inscripciones|null findOneBy(array $criteria, array $orderBy = null)
 * @method Inscripciones[]    findAll()
 * @method Inscripciones[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InscripcionesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Inscripciones::class);
    }

    // /**
    //  * @return Inscripciones[] Returns an array of Inscripciones objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Inscripciones
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
