<?php

namespace App\Repository;

use App\Entity\Post;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\FetchMode;
use PDO;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Post::class);
    }

    // /**
    //  * @return Post[] Returns an array of Post objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Post
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function filtrosComplejos($fecha1, $fecha2, $tipo, $texto,User $user)
    {
        $sql = $this->createQueryBuilder('p')
            ->innerJoin('p.user', 'us');

        $parametros = [];
        if ($user->getRole() == 'ROLE_ADMIN')
        {
            $sql->where($sql->expr()->eq('1', '1'));
        }
        else
        {
            $sql->where($sql->expr()->eq('us.id', $user->getId()));
        }
        if (isset($fecha1) && isset($fecha2) && !empty($fecha1) && !empty($fecha2))
        {
            if ($fecha1 > $fecha2)
            {
                $fechaSQL1 = $fecha2;
                $fechaSQL2 = $fecha1;
            }
            else
            {
                $fechaSQL1 = $fecha1;
                $fechaSQL2 = $fecha2;
            }
            $sql->andWhere($sql->expr()->between('p.fecha', ':FECHA1', ':FECHA2'));
            $parametros[':FECHA1'] = trim(htmlspecialchars($fechaSQL1 . ' 00:00:00'));
            $parametros[':FECHA2'] = trim(htmlspecialchars($fechaSQL2 . ' 23:59:59'));
        }
        if (!empty($fecha1) && isset($fecha1) && empty($fecha2))
        {
            $sql->andWhere($sql->expr()->gte('p.fecha', ':FECHA1'));
            $parametros[':FECHA1'] = trim(htmlspecialchars($fecha1));
        }
        if (!empty($fecha2) && isset($fecha2) && empty($fecha1))
        {
            $sql->andWhere($sql->expr()->lte('p.fecha', ':FECHA2'));
            $parametros[':FECHA2'] = trim(htmlspecialchars($fecha2));
        }
        if (isset($tipo) && !empty($tipo))
        {
            $sql->andWhere($sql->expr()->like('p.tipo', ':TIPO'));
            $parametros[':TIPO'] = trim(htmlspecialchars(strtoupper($tipo)));
        }
        if (isset($texto) && !empty($texto))
        {
            $sql->andWhere($sql->expr()->like('p.tipo', $sql->expr()->upper(':TIPO')));
            $parametros[':TEXTO'] = "%".trim(htmlspecialchars(strtoupper($texto)))."%";
        }

        $sql->setParameters($parametros);

        $query = $sql->getQuery();

        return $query->execute();
    }

    public function filtrosComplejosInicio($fecha1, $fecha2, $tipo, $texto)
    {
        $sql = $this->createQueryBuilder('p')
            ->innerJoin('p.user', 'us');

        $parametros = [];
        $sql->where($sql->expr()->eq('1', '1'));

        if (isset($fecha1) && isset($fecha2) && !empty($fecha1) && !empty($fecha2))
        {
            if ($fecha1 > $fecha2)
            {
                $fechaSQL1 = $fecha2;
                $fechaSQL2 = $fecha1;
            }
            else
            {
                $fechaSQL1 = $fecha1;
                $fechaSQL2 = $fecha2;
            }
            $sql->andWhere($sql->expr()->between('p.fecha', ':FECHA1', ':FECHA2'));
            $parametros[':FECHA1'] = trim(htmlspecialchars($fechaSQL1 . ' 00:00:00'));
            $parametros[':FECHA2'] = trim(htmlspecialchars($fechaSQL2 . ' 23:59:59'));
        }
        if (!empty($fecha1) && isset($fecha1) && empty($fecha2))
        {
            $sql->andWhere($sql->expr()->gte('p.fecha', ':FECHA1'));
            $parametros[':FECHA1'] = trim(htmlspecialchars($fecha1));
        }
        if (!empty($fecha2) && isset($fecha2) && empty($fecha1))
        {
            $sql->andWhere($sql->expr()->lte('p.fecha', ':FECHA2'));
            $parametros[':FECHA2'] = trim(htmlspecialchars($fecha2));
        }
        if (isset($tipo) && !empty($tipo))
        {
            $sql->andWhere($sql->expr()->like('p.tipo', ':TIPO'));
            $parametros[':TIPO'] = trim(htmlspecialchars(strtoupper($tipo)));
        }
        if (isset($texto) && !empty($texto))
        {
            $sql->andWhere($sql->expr()->like('p.tipo', $sql->expr()->upper(':TIPO')));
            $parametros[':TEXTO'] = "%".trim(htmlspecialchars(strtoupper($texto)))."%";
        }

        $sql->setParameters($parametros);

        $query = $sql->getQuery();

        return $query->execute();
    }
}
