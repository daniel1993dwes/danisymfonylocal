'use strict';

document.addEventListener('DOMContentLoaded', event =>{
    let textarea = document.getElementById('postText');
    document.getElementById('parr').addEventListener('click', e =>{
        e.preventDefault();
        textarea.value += '<p></p>';
    })
    document.getElementById('span').addEventListener('click', e =>{
        e.preventDefault();
        textarea.value += '<span></span>';
    })
    document.getElementById('i').addEventListener('click', e =>{
        e.preventDefault();
        textarea.value += '<i></i>';
    })
    document.getElementById('sub').addEventListener('click', e =>{
        e.preventDefault();
        textarea.value += '<u></u>';
    })
    document.getElementById('neg').addEventListener('click', e =>{
        e.preventDefault();
        textarea.value += '<strong></strong>';
    })
    document.getElementById('listDes').addEventListener('click', e =>{
        e.preventDefault();
        textarea.value += '<ul><li>Elemento</li></ul>';
    })
    document.getElementById('h2').addEventListener('click', e =>{
        e.preventDefault();
        textarea.value += '<h2></h2>';
    })
    document.getElementById('h3').addEventListener('click', e =>{
        e.preventDefault();
        textarea.value += '<h3></h3>';
    })
    document.getElementById('quote').addEventListener('click', e =>{
        e.preventDefault();
        textarea.value += '<q></q>';
    })
    document.getElementById('spc').addEventListener('click', e =>{
        e.preventDefault();
        textarea.value += '<pre></pre>';
    })
})